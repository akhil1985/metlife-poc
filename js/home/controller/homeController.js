metApp.controller('homeController', function($scope, $http, $location, sharedProperties, $uibModal) {
    if (sharedProperties.getUserName() == "johnsmith1") {
        $scope.licenseExpired = true;
    } else {
        $scope.licenseExpired = false;
    }
    $http.get("data/database.json")
        .success(function(response) {
            $scope.leaddata = response.leaddata;
            $scope.mytask = response.mytask;
            $scope.recentactivities = response.recentactivities;
            $scope.quicklinks = response.quicklinks;
            $scope.myleads = response.myleads;
            if (sharedProperties.getQuotes() != null && sharedProperties.getQuotes().length != 0) {
                $scope.myleads[1].action = 'Install';
                $scope.myleads[1].leadscp[0].image = 'images/Progress-4.png'
            }
             if(sharedProperties.getUserName() == 'johnsmith1'){
                $scope.myleads[0].leadscp[0].image = 'images/Progress-4.png';
                $scope.myleads[0].leadscp[0].status = 'Install';
                $scope.myleads[1].leadscp[0].image = 'images/Progress-4.png';
                $scope.myleads[1].leadscp[0].status = 'Install';
                $scope.myleads[2].leadscp[0].image = 'images/Progress-4.png';
                $scope.myleads[2].leadscp[0].status = 'Install';

            }
        });
    $scope.show = 1;
    $scope.tabchange = function(tabno, actstatus) {
        $scope.show = tabno;
        $scope.actstatus = "active";
    };

    $scope.showQuotePopup = function(leadName, val) {

        if (leadName === 'Tony Yaa') {

            if (val === 'Install') {
                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'Quote.html',
                    controller: 'QuoteCartController',
                    size: 'lg'
                });
            } else {
                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'createQuote.html',
                    controller: 'QuoteController',
                    size: 'sm'
                });
            }
        }
    };

    $scope.opensubpage = function(data) {
        sharedProperties.setIndex(data);
        $location.path("/subhome");
    };

    $scope.homepage = function() {
        $location.path("/login");
        sharedProperties.setUserName('');
        sharedProperties.setIndex('');
        sharedProperties.setQuotes([]);

    };

    $scope.showCart = function() {
        $location.path("/nav");
        $scope.quote_url = "html/nav/cart.html";
    };
});
