metApp.controller('installController', function($scope, $http, $location) {
    
    $scope.backToDashBoard = function() {
        $location.path("/home");
    }
    
     $scope.logout = function() {
        $location.path("/login");
        sharedProperties.setUserName('');
        sharedProperties.setIndex('');
        sharedProperties.setQuotes([]);
    }
});
