metApp.controller('QuoteCartController', function($scope, $http, $location, sharedProperties, $uibModalInstance) {
    $scope.proceed=function(quote4, quote5, quote6){
		if(quote4 || quote5 || quote6) {
			$uibModalInstance.dismiss('cancel');
			$location.path('/install');
		}
    };

    $scope.quote1 = sharedProperties.getQuotes()[0];
    $scope.quote2 = sharedProperties.getQuotes()[1];
    $scope.quote3 = sharedProperties.getQuotes()[2];
    
     $scope.cancel=function(){
        $uibModalInstance.dismiss('cancel');
    };
});
